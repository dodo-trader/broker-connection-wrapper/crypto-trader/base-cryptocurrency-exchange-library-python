# Introduction

This repository tries to follow the standards mentioned below:

[Semantic Versioning 2.0.0](https://semver.org/)

## Getting Started

1. Clone the repository

    - Using SSH
    ```shell script
    git clone ssh://git@gitlab.com/aenco/libraries/base-crypto-currency-exchange-library-py
    ```
    - Using HTTPS with Personal Access Token
    ```shell script
    git clone https://{username}:{personal_acess_token}@gitlab.com/aenco/libraries/base-crypto-currency-exchange-library-py
    ```

1. Setup the Virtual Environment

    Ubuntu 18.04 (Debian-based Linux)
    ```shell script
    cd ./base-crypto-currency-exchange-library-py
    python3.8 -m venv venv/
    source venv/bin/activate
    ```

    Windows 10
    ```shell script
    cd .\base-crypto-currency-exchange-library-py
    python -m venv .\venv\
    .\venv\Scripts\activate
    ```

1. Install the dependencies

    ```shell script
    pip install -r requirements.txt
    ```
 
1. Uninstall package dependencies

   ```shell script
   pip uninstall -r requirements.txt -y
   ```

## Testing
 
[pytest](https://docs.pytest.org/en/latest/) is the preferred testing framework. Familiarise yourself with pytest first.

For Jetbrains PyCharm users, a run configuration have been setup for your convenience.

### Example

1. Running all test suite i.e. python files with the naming pattern test_*.py

    ```shell script
    pytest
    ```

1. Running tests

    Using tox
    ```shell script
    tox
    ```
    Note: Visit [tox](https://tox.readthedocs.io/en/latest/example/basic.html) for more information.
  
    Test suite
    ```shell script
    pytest test/unit_test/test_cryptocurrency.py
    ```
   
    Test function
    ```shell script
    pytest test/unit_test/test_cryptocurrency.py::test_initialise_cryptocurrency_with_recognised_crypto_type
    ```
   
   Note: Visit [pytest usage](https://docs.pytest.org/en/stable/usage.html) for more information.
