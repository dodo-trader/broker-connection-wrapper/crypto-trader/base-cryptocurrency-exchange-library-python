from setuptools import setup, find_packages

version = {}
with open("basecryptoexpy/version.py") as file:
    exec(file.read(), version)

setup(
    name='basecryptoexpy',
    version=version['__version__'],
    author='',
    author_email='',
    description="",
    url="https://gitlab.com/ernest-libraries/crypto-trader/base-cryptocurrency-exchange-library-python",
    zip_safe=False,
    packages=find_packages(),
    install_requires=[

    ],
)
